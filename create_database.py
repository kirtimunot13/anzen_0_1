import pymongo

myclient1 = pymongo.MongoClient("mongodb://localhost:27017/")
mydb1 = myclient1['Client_Name']
sign_up_collection = mydb1['Sign Up Questions']

sign_up = [
    {
        "question" : "Organisation ID",
        "options" : [],
    },

    {
        "question": "Enter your name",
        "options" : [],
    },

    {
        "question": "Password",
        "answer": [],
    },

    {
        "question": "Verify Password",
        "answer": [],
    },
    {
        "question": "Phone number",
        "answer": [],
    },
    {
        "question": "Age",
        "answer": [],
    },
    {
        "question": "Role",
        "answer": ["Admin","Security","Employee"],
    },
]
sign_up_collection.insert_many(sign_up)