from gevent import monkey
import csv
monkey.patch_all()

from flask import Flask, render_template, url_for, send_file, jsonify, request, session, send_from_directory, redirect
from flask_socketio import SocketIO, emit, join_room, leave_room
from flask_session import Session
import time
import datetime
from werkzeug.middleware.proxy_fix import ProxyFix
import json

app = Flask(__name__)
app.config['SECRET_KEY'] = 'top-secret!'
app.config['SESSION_TYPE'] = 'filesystem'
Session(app)
# socketio = SocketIO(app, manage_session=False)
# app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_proto=1)
socketio = SocketIO(app, manage_session=False, cors_allowed_origins="*")

import database_operations
import otp_operations


@socketio.on("connected")
def connected():
    print("connected")


@socketio.on("Log In")
def Log_In():
    print("Log In")


@socketio.on("check_user")
def check_user(data):
    room = data["phone"]
    join_room(room)
    session["user"] = data["phone"]
    session['o_id'] = data['o_id']
    session['login'] = False
    print(session)
    Val = database_operations.check_user(data)
    if Val == 0:
        socketio.emit("check_user_status", "User exists", room=room)
    else:
        socketio.emit("check_user_status", "User does not exist", room=room)


@socketio.on("Sign_Up_Request_1")
def sign_up_request_1(data):
    room = data["phone"]
    join_room(room)
    session["user"] = data["phone"]
    session['o_id'] = data['o_id']
    session['login'] = False
    print(session)
    Val = database_operations.signup_user(data)
    if Val == True:
        socketio.emit("sign_up_status", "Verify your number using the OTP we just shared with you.", room=room)
    else:
        socketio.emit("sign_up_status", "This number already exists in organisation.", room=room)


@socketio.on("Sign_Up_Request")
def sign_up_request(data):
    room = session['user']
    join_room(room)
    data["phone"] = session["user"]
    session['name'] = data["name"]
    # data['o_id'] = session['o_id']
    session['login'] = False
    print(session)
    Val = database_operations.signup_user_data(data)
    if Val == True:
        socketio.emit("sign_up_status", "Redirecting ...", room=room)
    else:
        socketio.emit("sign_up_status", "This name and number already exists in organisation.", room=room)


@socketio.on("verify_otp_data")
def verify_otp_data(data):
    print("In verify otp data")
    if 'user' in session:
        phone = session['user']
        room = session['user']
        join_room(room)
        otp = database_operations.return_otp(phone)
        print(str(otp['otp']))
        print(data)
        if str(otp['otp']) == str(data):
            database_operations.update_otp_verification(phone)
            socketio.emit("otp_status", "Redirecting ...", room=room)
            print("OTP successful")
        else:
            print("OTP not succesful but user found")
            socketio.emit("otp_status", "Verification not successful.", room=room)
        leave_room(room)
    else:
        room2 = request.remote_addr
        join_room(room2)
        print("OTP not succesful")
        socketio.emit("otp_status", "Verification not successful.", room=room2)
        leave_room(room2)


@socketio.on("verify_otp_data_partial")
def verify_otp_data_partial(data):
    print("In verify otp data")
    if 'user' in session:
        phone = session['user']
        room = session['user']
        join_room(room)
        otp = database_operations.return_otp(phone)
        print(str(otp['otp']))
        print(data)
        if str(otp['otp']) == str(data):
            database_operations.update_otp_verification_partial(phone)
            socketio.emit("otp_status", "Redirecting ...", room=room)
            print("OTP successful")
        else:
            print("OTP not succesful but user found")
            socketio.emit("otp_status", "Verification not successful.", room=room)
        leave_room(room)
    else:
        room2 = request.remote_addr
        join_room(room2)
        print("OTP not succesful")
        socketio.emit("otp_status", "Verification not successful.", room=room2)
        leave_room(room2)


@socketio.on("resend_otp")
def resend_otp():
    if 'user' in session:
        phone = session['user']
        print("Found User")
        Val = database_operations.update_otp(phone)
        print("Update requested")
    else:
        room2 = request.remote_addr
        join_room()
        socketio.emit("otp_status", "Verification not successful.", room=room2)
        leave_room(room2)


@socketio.on("login_cred")
def login_cred(data):
    room = data['phone']
    join_room(room)
    print(data)
    Val = database_operations.verify_credentials(data)
    if Val == 2:
        session["user"] = data["phone"]
        # session['role'] = data["role"]
        data1 = database_operations.return_credentials(data['phone'])
        session['role'] = data1['role']
        session['name'] = data1['name']
        session['o_id'] = data1['o_id']
        session["login"] = True
        socketio.emit("login_status", "2", room=room)
        leave_room(room)
    if Val == 1:
        session["user"] = data["phone"]
        # session['role'] = data["role"]
        data1 = database_operations.return_credentials(data['phone'])
        session['role'] = data1['role']
        session['name'] = data1['name']
        session['o_id'] = data1['o_id']
        session["login"] = False
        socketio.emit("login_status", "1", room=room)
    if Val == 0:
        socketio.emit("login_status", "0", room=room)


@socketio.on("login_cred_check")
def login_cred_check(data):
    room = data['phone']
    join_room(room)
    print(data)
    Val = database_operations.verify_credentials(data)
    if Val == 2:
        session["user"] = data["phone"]
        # session['role'] = data["role"]
        data1 = database_operations.return_credentials(data['phone'])
        session['role'] = data1['role']
        session['name'] = data1['name']
        session['o_id'] = data1['o_id']
        session["login"] = True
        socketio.emit("login_status_check", "2", room=room)
        leave_room(room)
    if Val == 1:
        session["user"] = data["phone"]
        # session['role'] = data["role"]
        data1 = database_operations.return_credentials(data['phone'])
        session['role'] = data1['role']
        session['name'] = data1['name']
        session['o_id'] = data1['o_id']
        session["login"] = False
        socketio.emit("login_status_check", "1", room=room)
    if Val == 0:
        socketio.emit("login_status_check", "0", room=room)


@app.route('/autologin', methods=['GET'])
def autologin():
    data = {}
    data['phone'] = request.args.get('phone')
    data['password'] = request.args.get('password')

    print("----------------------------")
    print(data)
    print("----------------------------")
    # room = data["phone"]
    # join_room(room)
    # print(data)
    Val = database_operations.verify_credentials(data)
    if Val == 2:
        session["user"] = data["phone"]
        # session['role'] = data["role"]
        data1 = database_operations.return_credentials(data['phone'])
        session['role'] = data1['role']
        session['name'] = data1['name']
        session['o_id'] = data1['o_id']
        session["login"] = True
        # socketio.emit("login_status","2",room=room)

        # -------------------------------------------------------------------------

        if 'user' in session and 'login' in session:
            if session['login']:
                return redirect(url_for('epass'))
                '''color="info"
                phone = session['user']
                epass = database_operations.get_epass(phone)
                #print(epass)
                timestamp = ""
                button_text = ""
                if "timestamp" in  epass :
                    timestamp ="Last record on : "+  epass['timestamp'].strftime("%d/%m/%Y %H:%M:%S")
                    if (datetime.datetime.now() + datetime.timedelta(minutes=330) - epass['timestamp'] ) > datetime.timedelta(1):
                        #print("1 day elapsed")
                        epass['epass_status']="No Application found in last 24 hours."
                        epass['qrcode_link'] = '/epass/fillup.jpg'
                        #print(timestamp)
                        color = "warning"
                        button_text = "Apply for E-Pass"
                    if epass['epass_status'] == 'Accepted' :
                        color = "success"
                        button_text = "Re-apply for E-Pass"
                    if epass['epass_status'] == 'Rejected':
                        color = "danger"
                        button_text = "Retry for E-Pass"
                    #print(color)
                else :
                    epass['epass_status'] = "You do not have any E-Pass Applications. Please answer questions in Self-Declaration section to get an E-Pass"
                    epass['qrcode_link'] = '/epass/fillup.jpg'
                    button_text = "Go to Self-Declaration "
					
			
				
				
                return render_template("epass.html",status=epass['epass_status'],
                                       last_app=timestamp,
                                       src=epass["qrcode_link"],
                                       color=color,
                                       active_page ='epass',
                                       role=session['role'],
                                       name=session['name'],
                                       button_text = button_text
                                       )'''


        else:
            return redirect(url_for('login'))

        # ----------------------------------------------------------------------------------------------------------------------

        # leave_room(room)
    if Val == 1:
        session["user"] = data["phone"]
        # session['role'] = data["role"]
        data1 = database_operations.return_credentials(data['phone'])
        session['role'] = data1['role']
        session['name'] = data1['name']
        session['o_id'] = data1['o_id']
        session["login"] = False
        # render_template('sign.html')
        return redirect(url_for('login'))
        ###return render_template("signup.html")
        # socketio.emit("login_status", "1",room=room)
    if Val == 0:
        # socketio.emit("login_status", "0",room=room)
        return redirect(url_for('login'))


@socketio.on("forgot_generate_otp")
def forgot_generate_otp(phone):
    room = phone
    join_room(phone)
    Val = database_operations.update_otp(phone)
    print("Update requested")
    leave_room(phone)
    # Send otp


@socketio.on("forgot_verify_otp")
def forgot_verify_otp(data):
    room = data["phone"]
    join_room(room)
    otp = database_operations.return_otp(data['phone'])
    if str(otp['otp']) == str(data['otp']):
        socketio.emit("otp_status", "Redirecting ...", room=room)
        print("OTP successful")
        session['user'] = data['phone']
        leave_room(room)
    else:
        print("OTP not succesful but user found")
        socketio.emit("otp_status", "Verification not successful.", room=room)


@socketio.on("reset_password")
def forgot_verify_otp(password):
    if 'user' in session:
        room = session['user']
        join_room(room)
        data = {}
        data['phone'] = session['user']
        data['password'] = password
        print("Found User")
        Val = database_operations.reset_password(data)
        if Val == True:
            socketio.emit("update_pass_status", "True", room=room)
            leave_room(room)
        else:
            socketio.emit("update_pass_status", "False", room=room)


@socketio.on("logout")
def logout():
    if 'user' in session:
        session.pop('user')
        session.pop("role")
        session.pop("name")
        session.pop('o_id')
        session.pop('login')
        print("Logged Out")
    else:
        print("User not found !")


@socketio.on("declaration_data")
def declaration(data):
    print(data)
    if 'user' in session:
        phone = session['user']
        room = phone
        join_room(room)
        Val = database_operations.update_declaration(phone, data, session)
        time.sleep(1)
        socketio.emit("declaration_submitted", room=room)
        # leave_room(room)


@app.route('/template')
def template():
    return render_template("template.html")


# ===================================================

@app.route('/visitor')
def visitor():
    return render_template("visitor.html")


@app.route('/visitor_epass')
def visitor_epass():
    return render_template("visitor_epass.html")


@socketio.on("visitor_Request")
def visitor_Request(dt):
    room = request.sid
    join_room(room)
    print("Data Requested")
    print(dt)
    status, link = database_operations.visitor_request(dt)
    socketio.emit("visitor_application", {"status": status, "link": link}, room=room)


@app.route('/temperature')
def temperature():
    if 'user' in session and 'login' in session:
        if session['login']:
            user_temp = database_operations.retrive_user_temp(session['user'])
            user_flag = True
            if len(user_temp) == 1:
                user_flag = False
            return render_template("temperature.html",
                                   active_page='temperature',
                                   role=session['role'],
                                   name=session['name'],
                                   user_temp=user_temp,
                                   user_flag=user_flag
                                   )
    else:
        return redirect(url_for('login'))


# ===================================================


@app.route('/epass/<path:filename>')
def download_file(filename):
    print('correct loop')
    return send_from_directory('code', filename, as_attachment=True)


@app.route('/login')
def login():
    if 'user' in session:
        phone = session['user']
        credentials = database_operations.return_credentials(phone)
        return render_template("signin.html", phone=phone, password=credentials['password'],
                               role=credentials['role'], o_id=credentials['o_id'])
    else:
        return render_template("signin.html")


@app.route('/createaccount')
def createaccount():
    return render_template("createaccount.html")


@app.route('/signup')
def signup():
    questions = database_operations.get_signup_questions()
    return render_template("signup.html")


@app.route('/signup1')
def signup1():
    return render_template("signup1.html")


@app.route('/signup2')
def signup2():
    return render_template("signup2.html")


@app.route('/verifyotp')
def verifyotp():
    if 'user' in session:
        print(session['user'])
        return render_template("verifyotp.html", user=session["user"])
    else:
        return render_template("signup.html")


@app.route('/forgotpassword')
def forgotpassword():
    return render_template("forgotpassword.html")


@app.route('/newpassword')
def newpassword():
    if 'user' in session:
        return render_template("newpassword.html")
    else:
        return render_template("signup.html")


@app.route('/epass')
def epass():
    if 'user' in session and 'login' in session:
        if session['login']:
            color = "info"
            phone = session['user']
            epass = database_operations.get_epass(phone)
            # print(epass)
            timestamp = ""
            button_text = ""
            new_timestamp = ""
            if "timestamp" in epass:
                timestamp = "Last record on : " + epass['timestamp'].strftime("%d/%m/%Y %H:%M:%S")
                new_timestamp = epass['timestamp'].strftime("%d %B, %Y")
                print("---------------------------", new_timestamp)
                # if (datetime.datetime.now() + datetime.timedelta(minutes=330).date() != epass['timestamp'].date() ) > datetime.timedelta(minutes=5):
                if (datetime.datetime.now() + datetime.timedelta(minutes=330)).date() != epass['timestamp'].date():
                    # print("1 day elapsed")
                    epass['epass_status'] = "No Application found in last 24 hours."
                    epass['qrcode_link'] = '/epass/fillup.jpg'
                    # print(timestamp)
                    color = "warning"
                    button_text = "Apply for E-Pass"
                if epass['epass_status'] == 'Accepted':
                    color = "success"
                    button_text = "Re-apply for E-Pass"
                if epass['epass_status'] == 'Rejected':
                    color = "danger"
                    button_text = "Retry for E-Pass"
                # print(color)
            else:
                epass[
                    'epass_status'] = "You do not have any E-Pass Applications. Please answer questions in Self-Declaration section to get an E-Pass"
                epass['qrcode_link'] = '/epass/fillup.jpg'
                button_text = "Go to Self-Declaration "

            scan_time = "--"
            temperature = "-"
            scan_unit = "-"
            print(session["o_id"])
            val, scan_settings_data = database_operations.get_oid_settings(session["o_id"])

            if val == 1:
                scan_unit = scan_settings_data["scan_unit"]
                scan_threshold = float(scan_settings_data["threshold_scan"])
                val, temperature_data = database_operations.get_latest_temmperature(phone)
                if val == 1:
                    temperature = temperature_data["temperature"]
                    scan_time = temperature_data["timestamp"].strftime("%d/%m/%Y :: %H:%M:%S")
                    print(temperature)
                    if int(temperature_data["temperature"]) > scan_threshold:
                        if (datetime.datetime.now() + datetime.timedelta(minutes=330)).date() == temperature_data[
                            "timestamp"].date():
                            epass["epass_status"] = "high_temperature"

            print(epass['qrcode_link'])

            return render_template("epass.html", status=epass['epass_status'],
                                   last_app=new_timestamp,
                                   scan_time=scan_time,
                                   src=epass["qrcode_link"],
                                   color=color,
                                   active_page='epass',
                                   role=session['role'],
                                   name=session['name'],
                                   button_text=button_text,
                                   temperature=temperature,
                                   scan_unit=scan_unit
                                   )
    else:
        return redirect(url_for('login'))


@app.route('/declaration')
def declaration():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("declaration.html",
                                   active_page='declaration',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@app.route('/distancing')
def distancing():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("distancing.html",
                                   active_page='distancing',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@app.route('/security')
def secrutiy():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("security.html",
                                   active_page='security',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@app.route('/manualsecurity')
def manualsecrutiy():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("manual_security.html",
                                   active_page='security',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@app.route('/contact')
def contact():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("contact.html",
                                   active_page='contact',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@app.route('/admin')
def admin():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("admin.html",
                                   active_page='admin',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


# --------------------------------------trace ----------------------------------
@app.route('/trace')
def trace():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("trace.html",
                                   active_page='trace',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))



@socketio.on("trace")
def trace_data(input):
    if 'user' in session and 'login' in session:
        if session['login']:
            room = session['user']
            join_room(room)
            val, data = database_operations.trace_data(input, session['o_id'])
            #r'C:\Users\HOME\PycharmProjects\anzen_1_0\contacttracing.json'
            if val == True:
                print("trace_data")
                socketio.emit("trace_data", data, room=room)
            else:
                socketio.emit("trace_data_status", "Employee not found", room=room)
   # socketio.emit("trace_data", jsondata)

@socketio.on("trace_data1")
def trace_data1():
    print("in trace_data1")
    if 'user' in session and 'login' in session:
        if session['login']:
            room = session['user']
            join_room(room)
            #r'C:\Users\HOME\PycharmProjects\anzen_1_0\contacttracing.json'
            with open(r'C:\Users\HOME\PycharmProjects\anzen_1_0\contacttracing.json') as json_file:
                jsondata = json.load(json_file)
                print(jsondata)
                print("trace_data")
           # socketio.emit("trace_data", jsondata, room=room)
        socketio.emit("trace_data", jsondata)
# -----------------------------attendance--------------------------------------------

@app.route('/attendance')
def attendance():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("attendance.html",
                                   active_page='attendance',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@socketio.on("attendance_data")
def attendance_data(flag):
    if flag == "null":
        if 'user' in session:
            room = session['user']
            join_room(room)

            current_time = datetime.datetime.now() #+ datetime.timedelta(minutes=330)
            to_date = current_time.strftime("%Y-%m-%d")
            one_day_prior = current_time - datetime.timedelta(hours=24)
            from_date = one_day_prior.strftime("%Y-%m-%d")
            to_send = {"from_date": from_date, "to_date": to_date}
            socketio.emit("set_date", to_send, room=room)
            print(to_send)
            data = database_operations.attendance_data(session['o_id'], to_send)
            print(database_operations.attendance_data,session['o_id'])
            socketio.emit("reciv_attendance", data, room=room)
    else:
        if 'user' in session:
            room = session['user']
            join_room(room)
            data = database_operations.attendance_data(session['o_id'], flag)
            print(data, session['o_id'])
            socketio.emit("reciv_attendance", data, room=room)


@socketio.on("modal_attendance")
def modal_attendance(data):
    if 'user' in session:
        room = session['user']
        join_room(room)
        data = database_operations.attendance_modal(data, session['o_id'])

        socketio.emit("modal_data", data, room=room)


@socketio.on("date_wise_attendance")
def date_wise_attendance(date):
    if 'user' in session:
        room = session['user']
        join_room(room)

        format_str = '%d/%m/%Y'  # The format
        start_time = datetime.datetime.strptime(date, format_str)
        print(start_time.date())

        '''socketio.emit("set_date", date, room=room)

        data = database_operations.date_wise_attendance(date,session['o_id'])
        socketio.emit("reciv_attendance",data,room=room)'''


# ------------------------------visitors-----------------------------------

@app.route('/visitors')
def visitors():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("visitors.html",
                                   active_page='visitors',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@socketio.on("visitor_data")
def visitor_data():
    if 'user' in session:
        room = session['user']
        join_room(room)

        current_time = datetime.datetime.now() + datetime.timedelta(minutes=330)
        date = current_time.strftime("%d/%m/%Y")
        socketio.emit("set_date", date, room=room)

        data = database_operations.visitor_data(session['o_id'])
        socketio.emit("reciv_visitor", data, room=room)


@socketio.on("modal_visitor")
def modal_visitor(data):
    if 'user' in session:
        room = session['user']
        join_room(room)
        data = database_operations.visitor_modal(data, session['o_id'])
        print(data)
        socketio.emit("modal_data_visitor", data, room=room)


@socketio.on("date_wise_visitor")
def date_wise_visitor(date):
    if 'user' in session:
        room = session['user']
        join_room(room)
        socketio.emit("set_date", date, room=room)

        data = database_operations.date_wise_visitor(date, session['o_id'])
        socketio.emit("reciv_visitor", data, room=room)


# --------------------------------------------Applications-----------------------

@app.route('/applications')
def application():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("application.html",
                                   active_page='applications',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@socketio.on("application_data")
def application_data():
    if 'user' in session:
        room = session['user']
        join_room(room)

        data = database_operations.application_data(session['o_id'])
        socketio.emit("application", data, room=room)


@socketio.on("change_status")
def change_status(arr):
    if 'user' in session:
        room = session["user"]
        join_room(room)
        data = database_operations.change_status(arr, session['o_id'])
        # socketio.emit("application",data,room=room)


@socketio.on("question")
def cquestion(question):
    if 'user' in session:
        room = session["user"]
        join_room(room)
        data = database_operations.log_question(question, session)
        # socketio.emit("application",data,room=room)


@socketio.on("modal_application")
def modal_application(x):
    if 'user' in session:
        room = session["user"]
        join_room(room)
        data = database_operations.modal_application(x, session['o_id'])
        socketio.emit("modal_data_application", data, room=room)


@socketio.on("updated_role")
def updated_role(arr):
    if 'user' in session:
        room = session["user"]
        data = database_operations.update_role(arr, session['o_id'])
        return "ok"


@socketio.on("updated_employee_status")
def updated_employee_status(arr):
    print(arr)
    if 'user' in session:
        room = session["user"]
        data = database_operations.update_employee_type(arr, session['o_id'])
        return "ok"


# ==========================================================================

@socketio.on("manual_security")
def manual_security(data):
    if 'user' in session:
        room = session["user"]
        join_room(room)
        val, temp_ok = database_operations.manual_temp_entry(data, session)
        if val == False:
            socketio.emit("manual_scurity_status", "not_a_person", room=room)
        else:
            if temp_ok == False:
                socketio.emit("manual_scurity_status", "temp_high", room=room)
            if temp_ok == True:
                socketio.emit("manual_scurity_status", "temp_ok", room=room)
            # print(data)


@socketio.on("scanned_security")
def scanned_security(data):
    if 'user' in session:
        room = session["user"]
        join_room(room)
        val, temp_ok = database_operations.scanned_security(data, session)
        if val == False:
            socketio.emit("manual_scurity_status", "not_a_person", room=room)
            socketio.emit("manual_scurity_status", "not_a_person")
        else:
            if temp_ok == False:
                socketio.emit("manual_scurity_status", "temp_high", room=room)
                socketio.emit("manual_scurity_status", "temp_high", room=room)
            if temp_ok == True:
                socketio.emit("manual_scurity_status", "temp_ok", room=room)
        leave_room(room)

        if val == False:
            socketio.emit("broadcast_scurity_status", "not_a_person")
            socketio.emit("broadcast_scurity_status", "not_a_person")
        else:
            if temp_ok == False:
                socketio.emit("broadcast_scurity_status", "temp_high")
                socketio.emit("broadcast_scurity_status", "temp_high")
            if temp_ok == True:
                socketio.emit("broadcast_scurity_status", "temp_ok")


# Additional Post Event for Android Scanning Activity
@app.route('/scan_android', methods=['POST', 'GET'])
def scanned_android():
    data = {}
    session = {}
    session['name'] = "Android App"
    if request.method == 'POST':
        print(request.json)
        data = request.json
        '''if data == None
            if 'uid' and  'temperature'
            data['uid'] = request.form['uid']
            data['temperature'] = request.form['temperature']
            data['mask'] = request.form['mask']'''
        print("------Scan Recieved-----")
        if "uid" in data:
            if len(data['uid']) > 32:
                data['uid'] = (data['uid'][0:32]).lower()
                print(data["uid"])
                print("------------------------")
                val, temp_ok = database_operations.scanned_security(data, session)
                if val == False:
                    return jsonify({"response": "not_a_person"})
                else:
                    if temp_ok == False:
                        return jsonify({"response": "temp_high"})

                    if temp_ok == True:
                        return jsonify({"response": "temp_ok"})
            else:
                return jsonify({"response": "Invalid QUERY"})
    # return "OK"

    # return redirect(url_for('success',name = user))


# Additional Post Event for Android Scanning Activity
@app.route('/scan_hardware', methods=['POST', 'GET'])
def scan_hardware():
    data = {}
    session = {}
    session['name'] = "C4i4 Hardware"
    if request.method == 'GET':
        record = request.args.get('record')
        print("------------------")
        print("GET EVENT RECEIVED")
        print(record)
        if len(record) >= 50:
            uid = record[0:32]
            temperature = record[32:36]
            date = record[36:50]

            print(uid)
            print(temperature)
            print(date)

            format_str = '%d%m%Y%H%M%S'  # The format
            timestamp = datetime.datetime.strptime(date, format_str)
            # print(datetime_obj.)

            data['uid'] = uid
            data['temperature'] = float(temperature[0:2] + "." + temperature[2:4])
            data['mask'] = "yes"
            print("------------------------")
            val, temp_ok = database_operations.scanned_hardware(data, session, timestamp)
            print("------------------")
            if val == False:
                # return jsonify({"response": "not_a_person"})
                return "0"
            else:
                if temp_ok == False:
                    # return jsonify({"response": "temp_high"})
                    return "1"
                if temp_ok == True:
                    # return jsonify({"response": "temp_ok"})
                    return "1"
        else:
            # return jsonify({"response": "INVALID QUERY"})
            return "0"


# ===========================================================================

@app.route('/masterlogin')
def masterlogin():
    return render_template("master-signin.html")


@socketio.on("master_login_credentials")
def master_login_credentials(data):
    room = request.sid
    join_room(room)
    print("Master Login Credentials received")
    if data["phone"] == "Admin" and data["password"] == "Anzen2020":
        socketio.emit("master_credentials_validation", "1", room=room)
        session["master_user"] = True
    else:
        socketio.emit("master_credentials_validation", "0", room=room)
        session["master_user"] = False


# ----------------- MAster Application -------------
@app.route('/master-applications')
def masterapplications():
    if 'master_user' in session:
        if session['master_user']:
            return render_template("master-application.html",
                                   active_page='master-applications',
                                   )
    else:
        return redirect(url_for('masterlogin'))


@socketio.on("master_application_data")
def master_application_data():
    if 'master_user' in session:
        room = request.sid
        join_room(room)

        data = database_operations.master_application_data()
        socketio.emit("master_application", data, room=room)


@socketio.on("master_change_status")
def master_change_status(arr):
    if 'master_user' in session:
        room = request.sid
        join_room(room)
        data = database_operations.master_change_status(arr)
        socketio.emit("master_application", data, room=room)


@socketio.on("master_modal_application")
def master_modal_application(x):
    if 'master_user' in session:
        room = request.sid
        join_room(room)
        data = database_operations.master_modal_application(x)
        socketio.emit("master_modal_data_application", data, room=room)


@socketio.on("master_updated_role")
def master_updated_role(arr):
    if 'master_user' in session:
        room = request.sid
        print("Requested Role Update")
        data = database_operations.master_update_role(arr)
        return "ok"


@socketio.on("master_updated_employee_status")
def updated_employee_status(arr):
    print(arr)
    if 'master_user' in session:
        room = request.sid
        print("Reqested Employee Status Updated ")
        data = database_operations.master_update_employee_type(arr)
        return "ok"


@socketio.on("master_logout")
def master_logout():
    if 'master_user' in session:
        session.pop('master_user')
        print("Master Logged Out")
    else:
        print("User not found !")


@socketio.on("save_question_changes")
def save_question_changes(data):
    if 'master_user' in session:
        room = request.sid
        data, val = database_operations.save_questions(data)
        socketio.emit("master_console", "Saved !", room=room)
    else:
        print("User not found !")


@socketio.on("customer_onboarding")
def customer_onboarding(data):
    if 'master_user' in session:
        room = request.sid
        data, val = database_operations.on_board_customer(data)
        if data == 0:
            socketio.emit("master_console", "Customer with Same Organisation ID exists !", room=room)
        else:
            socketio.emit("master_console", "Congratulations ! New Customer On-boarded .", room=room)
    else:
        print("User not found !")


@app.route('/master-onboarding')
def master_onboarding():
    if 'master_user' in session:
        if session['master_user']:
            return render_template("master-onboarding.html",
                                   active_page='master-onboarding',
                                   )
    else:
        return redirect(url_for('masterlogin'))


@app.route('/master-questions')
def master_questions():
    if 'master_user' in session:
        if session['master_user']:
            return render_template("master-questions.html",
                                   active_page='master-questions',
                                   )
    else:
        return redirect(url_for('masterlogin'))


@socketio.on("get_master_questions")
def get_master_questions():
    if 'master_user' in session:
        room = request.sid
        join_room(room)
        data = database_operations.get_questions()
        print("--------------------------")
        print(data)
        print("--------------------------")

        socketio.emit("sent_master_questions", data, room=room)


@app.route('/all_questions')
def all_questions():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template(".html",
                                   active_page='settings',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


# -----------------------------------------------------
@app.route('/settings')
def settings():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("settings.html",
                                   active_page='settings',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@socketio.on("settings_info")
def get_settings_info():
    if 'user' in session:
        room = session["user"]
        room = request.sid
        join_room(room)
        val, data = database_operations.get_settings(session['o_id'])
        print(data)
        socketio.emit("settings_info_return", data)
        leave_room(room)
        return "ok"


@socketio.on("change_settings")
def change_settings(data):
    if 'user' in session:
        room = session["user"]
        room = request.sid
        join_room(room)
        database_operations.change_settings(session['o_id'], data)
        socketio.emit("settings_saved")
        leave_room(room)
        return "ok"


# -----------------manual_user_entry------------------
@socketio.on("manual_user")
def manual_user(data):
    print(data)
    if 'user' in session:
        o_id = session['o_id']
        Val = database_operations.manual_user(data, o_id)
        socketio.emit("manual_user_response", str(Val))


@socketio.on("qrcode_request")
def qrcode_request():
    if 'user' in session:
        o_id = session['o_id']
        Val = database_operations.qrcode_list_request(o_id)
        socketio.emit("qrcode_list_received", Val)


@app.route('/qrcode')
def qrcode():
    return render_template("qrcode_list.html")


# -----------------------------------
# UPDATE
@app.route('/update')
def update():
    return render_template("update.html")


@app.route("/update_app", methods=['GET'])
def update_app():
    return render_template('update_app.html', value="APK Download")


@app.route('/return-files/<filename>')
def return_files_tut(filename):
    filepath = "Anzen_update.apk"
    return send_file(filepath, as_attachment=True, attachment_filename='Anzen_update.apk')


@socketio.on("get_daily_declaration_questions")
def get_daily_declaration_questions():
    if 'user' in session:
        o_id = session['o_id']
        room = request.sid
        join_room(room)
        data = database_operations.get_daily_questions(o_id)
        print("--------------------------")
        print(data)
        print("--------------------------")
        socketio.emit("sent_daily_declaration_questions", data, room=room)


@socketio.on("get_signup_declaration_questions")
def get_signup_declaration_questions():
    if 'user' in session:
        o_id = session['o_id']
        room = request.sid
        join_room(room)
        data = database_operations.get_signup_questions(o_id)
        print("--------------------------")
        print(data)
        print("--------------------------")
        socketio.emit("sent_signup_declaration_questions", data, room=room)


@socketio.on("declaration_data_new")
def declaration_new(data):
    print("--------------------------")
    print(data)
    print("--------------------------")

    if 'user' in session:
        phone = session['user']
        room = phone
        join_room(room)
        Val = database_operations.update_declaration_new(phone, data, session)
        time.sleep(1)
        socketio.emit("declaration_submitted", room=room)
        # leave_room(room)


@socketio.on("organisation_settings_change")
def organisation_settings_change(data):
    if 'master_user' in session:
        room = request.sid
        data, val = database_operations.organisation_settings_change(data, session['o_id'])
        if data == 0:
            socketio.emit("master_console", "Settings not saved.", room=room)
        else:
            socketio.emit("master_console", "Settings saved successfully !", room=room)
    else:
        print("User not found !")


@socketio.on("get_settings_oid")
def get_settings_oid():
    if 'user' in session:
        o_id = session['o_id']
        room = request.sid
        data = database_operations.get_settings_oid(o_id)
        if data == []:
            pass
        else:
            socketio.emit("send_settings_oid", data, room=room)


# ------------------------------------

@app.route('/profile')
def profile():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("profile.html",
                                   active_page='profile',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@socketio.on("get_profile")
def get_profile():
    if 'user' in session:
        o_id = session['o_id']
        room = request.sid
        data = database_operations.get_profile(session['user'])
        if data == []:
            pass
        else:
            socketio.emit("send_profile", data, room=room)


@socketio.on("Profile_Update")
def profile_update(dataset):
    if 'user' in session:
        o_id = session['o_id']
        room = request.sid
        data = database_operations.update_profile(dataset, session['user'])
        socketio.emit("master_console", "Data saved successfully !", room=room)


# ----------------- Non Smartphone Users ----------------------------

@socketio.on("Sign_Up_Request_Non_Smartphone")
def sign_up_request_manual(data):
    room = session['user']
    join_room(room)
    data["o_id"] = session["o_id"]
    print(session)
    print(data)
    Val = database_operations.Sign_Up_Request_Manual(data)
    if Val == True:
        socketio.emit("sign_up_status", "Non Smartphone User Signed Up.", room=room)
    else:
        socketio.emit("sign_up_status", "This name and number already exists in organisation.", room=room)


@app.route('/manual_new')
def manual_new():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("non_smartphone.html",
                                   active_page='manual',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@app.route('/manual_all')
def manual_all():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("manual_all.html",
                                   active_page='manual',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@socketio.on("manual_application_data")
def manual_application_data():
    if 'user' in session:
        room = session['user']
        join_room(room)
        data = database_operations.manual_application_data(session['o_id'])
        socketio.emit("application", data, room=room)


@app.route('/manual_report')
def manual_report():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("Anzen.htm",
                                   active_page='manual',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@socketio.on("manual_report_data")
def manual_report_data():
    if 'user' in session:
        room = session['user']
        join_room(room)
        data = database_operations.manual_report_data(session['o_id'])
        socketio.emit("send_manual_report_data", data, room=room)


# --------------------------------------------------------------------------------------
# Contact Trace 

@app.route('/contact_trace_log', methods=['POST', 'GET'])
def contact_trace_log():
    if request.method == 'POST':
        return "wrong_method"
    else:
        data = {}
        data['phone'] = request.args.get('user_phone')
        data['uid'] = request.args.get('uid')
        data['rssi'] = request.args.get('rssi')
        data['timestamp'] = request.args.get('timestamp')
        data['uuid'] = request.args.get('uuid')
        data["social_distance"] = request.args.get('social_distance')
        data["total_contact"] = request.args.get('total_contact')
        data["team"] = request.args.get('team')
        data['pause'] = request.args.get('pause')

        print(" ------------------------------ ")
        print(" ------------------------------ ")
        print(data)
        print(" ------------------------------ ")
        print(" ------------------------------ ")

        return "data recieved"

# ===================================================
'''
NEW NOTIFICATIONS CODE
'''
# ===================================================
@app.route('/new_notifications', methods=['POST', 'GET'])
def new_notifications():
    data = {}
    session = {}
    session['name'] = "Android App"
    if request.method == 'POST':
        print(request.json)
        data = request.json
        print("------ Notification Request Recieved -----")
        print(data)
        print("-------------------------------------------")
        return jsonify({"response": "Notification Response"})

@app.route('/message_compose')
def message_compose():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("message_compose.html",
                                   active_page='notifications',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))

@socketio.on("message_compose_data")
def message_compose_data():
    if 'user' in session:
        room = session['user']
        join_room(room)
        data = database_operations.message_compose_data(session['o_id'])
        socketio.emit("message_application", data, room=room)


@socketio.on("composed_message")
def composed_message(data):
    if 'user' in session:
        room = session['user']
        join_room(room)
        #data = database_operations.composed_message(session['o_id'])
        print("-------------------COMPOSED MESSAGE -------------")
        print(data)
        print("----------------------------------------")

        data = database_operations.send_message(data,session)

        #socketio.emit("message_application", data, room=room)


# -------------------------- Register USer TOKEN from Device ------------------------
@app.route('/register_token', methods=['POST', 'GET'])
def register_token():
    data = {}
    session = {}
    session['name'] = "App"
    if request.method == 'POST':
        print(request.json)
        data = request.json
        print("------ Register TOKEN Request Recieved -----")
        print(data)
        val = database_operations.add_user_registration_token(data)
        print("-------------------------------------------")
        return jsonify({"response": "RECEIVED"})



#----------------------
# Question Settings
#----------------------


@app.route('/questions')
def oid_questions():
    if 'user' in session:
        if session['user']:
            return render_template("question_settings.html",
                                   active_page='questions',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@socketio.on("get_oid_questions")
def get_oid_questions():
    if 'user' in session:
        room = request.sid
        join_room(room)
        data = database_operations.get_questions_oid(session['o_id'])
        #print("--------------------------")
        #print(data)
        #print("--------------------------")
        to_send = data[0]["question_settings"]
        print("--------------------------")
        print(to_send)
        print("--------------------------")

        socketio.emit("sent_oid_questions",to_send, room=room)

@socketio.on("save_question_changes_oid")
def save_question_changes_oid(data):
    if 'user' in session:
        room = request.sid
        data, val = database_operations.save_questions_oid(data,session["o_id"])
        socketio.emit("master_console", "Saved !", room=room)
    else:
        print("User not found !")



#------- MESSAGE HISTORY ----------

@app.route('/notifications')
def notifications():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("all_notifications.html",
                                   active_page='notifications',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))

@socketio.on("get_all_messages")
def get_all_messages():
    if 'user' in session:
        room = request.sid
        val,data = database_operations.get_all_messages(session)
        if val == 0 :
            socketio.emit("master_console", "No messages found !", room=room)
        if val == 1:
            socketio.emit("send_all_messages",data, room=room)

@socketio.on("read_data_csv")
def attendance_data_csv(data):
    if 'user' in session:
        room = request.sid
        print("old_att",data)
        # name of csv file
        filename = "university_records1.csv"
        # writing to csv file
        with open(filename, 'w') as csvfile:
            # creating a csv writer object
            csvwriter = csv.writer(csvfile)

            # writing the data rows
            csvwriter.writerows(data)
        print("new_att :",data)
    emit('export_csv',data)


if __name__ == "__main__":
    socketio.run(app, host="0.0.0.0", port=80, debug=True)
