import pymongo
import json
import bson
from bson.json_util import dumps,loads
from bson import ObjectId, Binary, Code,json_util
from datetime import datetime



myclient1 = pymongo.MongoClient("mongodb://localhost:27017/")
mydb1 = myclient1['Anzen_1_0']
sign_up_collection = mydb1['Sign Up Questions']
question_data_collection = mydb1['all_question']

 

question_list=[
{"question":{
"que":"Do you have fever since yesterday?",
"type":"Options",
"options":["Yes","No"],
"q_id":"question1",
"correct/better_option":"No",
"sign_up":"no",
"Daily_declaration":"yes"}
},

{"question":{
"que":"Do you have cough since yesterday?",
"type":"Options",
"options":["Yes","No"],
"q_id":"question2",
"correct/better_option":"No",
"sign_up":"no",
"Daily_declaration":"yes"}
},

{"question":{
"que":"Do you have cold since yesterday?",
"type":"Options",
"options":["Yes","No"],
"q_id":"question3",
"correct/better_option":"No",
"sign_up":"no",
"Daily_declaration":"yes"}
},

{"question":{
"que":"Have you been in red containment zone since yesterday?",
"type":"Options",
"options":["Yes","No"],
"q_id":"question4",
"correct/better_option":"No",
"sign_up":"no",
"Daily_declaration":"yes"}
},

{"question":{
"que":"Do you have any family members less than 10 years of age or greater than 65 years of age ?",
"type":"Options",
"options":["Yes","No"],
"q_id":"question5",
"correct/better_option":"No",
"sign_up":"yes",
"Daily_declaration":"no"}
},
{"question":{
"que":"Do you have any of the following?",
"type":"Multiple_Options",
"options":["Heart Problems","Diabetes","Respiratory Problems","Blood Pressure","None of these"],
"q_id":"question6",
"correct/better_option":["None of these"],
"sign_up":"yes",
"Daily_declaration":"no"}
},

{"question":{
"que":"Email",
"type":"Single Entry",
"options":"",
"q_id":"question7",
"correct/better_option":"",
"sign_up":"no",
"Daily_declaration":"no"}
},

{"question":{
"que":"Department",
"type":"Single Entry",
"options":"",
"q_id":"question8",
"correct/better_option":"",
"sign_up":"no",
"Daily_declaration":"no"}
},

{"question":{
"que":"Age",
"type":"Single Entry",
"options":"",
"q_id":"question9",
"correct/better_option":"",
"sign_up":"no",
"Daily_declaration":"no"}
},

{"question":{
"que":"Your office is in (City)",
"type":"Single Entry",
"options":"",
"q_id":"question10",
"correct/better_option":"",
"sign_up":"no",
"Daily_declaration":"no"}
},

{"question":{
"que":"During lockdown have you travelled to any other city/state/country?",
"type":"Options",
"options":["Yes","No"],
"q_id":"question11",
"correct/better_option":"No",
"sign_up":"no",
"Daily_declaration":"no"}
},


{"question":{
"que":"Your residence (from where you travel to work everyday) is located in",
"type":"Options",
"options":["Containment Zone","Red Zone","Orange Zone","Green Zone"],
"q_id":"question12",
"correct/better_option":"Green Zone",
"sign_up":"no",
"Daily_declaration":"no"}
},

{"question":{
"que":"While coming to work everyday, What will be your mode of travel?",
"type":"Multiple_Options",
"options":["Walking","Bicycle","Scooter","Own Car","Car Pool","Cab/Taxi","Auto Rickshaw","Public Transport (Bus)","Public Transport (Train/Metro)"],
"q_id":"question13",
"correct/better_option":[],
"sign_up":"no",
"Daily_declaration":"no"}
},
{"question":{
"que":"What is the distance between your office and home? (in km)",
"type":"Single Entry",
"options":"",
"q_id":"question14",
"correct/better_option":"",
"sign_up":"no",
"Daily_declaration":"no"}
},
{"question":{
"que":"Did you had any symptoms of Common Flu during the lockdown period?",
"type":"Options",
"options":["Yes","No"],
"q_id":"question15",
"correct/better_option":"No",
"sign_up":"no",
"Daily_declaration":"no"}
},

{"question":{
"que":"Were you in close contact with a family member or any other person having Common Flu Symptoms OR COVID-19 Symptoms during the lockdown period?",
"type":"Options",
"options":["Yes","No"],
"q_id":"question16",
"correct/better_option":"No",
"sign_up":"no",
"Daily_declaration":"no"}
},

{"question":{
"que":"Were you in institutional or home quarantine during lockdown period?*",
"type":"Single Entry",
"options":"",
"q_id":"question17",
"correct/better_option":"",
"sign_up":"no",
"Daily_declaration":"no"}
},
{"question":{
"que":"Do you have any chronic medical condition (Co-morbidity)? [For Ex. Diabetes, Hypertension, Cardiovascular Disease (Heart Disease, Stroke etc.), Respiratory Disease (Asthma, COPD, TB etc.), Kidney Disease, Liver Disease, Cancer, etc] *",
"type":"Options",
"options":["Yes","No"],
"q_id":"question18",
"correct/better_option":"No",
"sign_up":"no",
"Daily_declaration":"no"}
},

{"question":{
"que":"Are you married?",
"type":"Options",
"options":["Yes","No"],
"q_id":"question19",
"correct/better_option":"",
"sign_up":"no",
"Daily_declaration":"no"}
},

{"question":{
"que":"Your Gender",
"type":"Single Entry",
"options":"",
"q_id":"question20",
"correct/better_option":"",
"sign_up":"no",
"Daily_declaration":"no"}
},

{"question":{
"que":"Is your wife pregnant?",
"type":"Options",
"options":["Yes","No"],
"q_id":"question21",
"correct/better_option":"",
"sign_up":"no",
"Daily_declaration":"no"}
},

{"question":{
"que":"Is your wife a nursing mother?",
"type":"Options",
"options":["Yes","No"],
"q_id":"question22",
"correct/better_option":"",
"sign_up":"no",
"Daily_declaration":"no"}
},

{"question":{
"que":"Is your wife suffering from Chronic Medical Condition? [For Ex. Diabetes, Hypertension, Cardiovascular Disease (Heart Disease, Stroke etc.), Respiratory Disease (Asthma, COPD, TB etc.), Kidney Disease, Liver Disease, Cancer etc] *",
"type":"Options",
"options":["Yes","No"],
"q_id":"question23",
"correct/better_option":"No",
"sign_up":"no",
"Daily_declaration":"no"}
},

{"question":{
"que":"Do you have children at home?",
"type":"Options",
"options":["Yes","No"],
"q_id":"question24",
"correct/better_option":"",
"sign_up":"no",
"Daily_declaration":"no"}
},

{"question":{
"que":"Are any of these children less than 10 years of age?",
"type":"Options",
"options":["Yes","Not Applicable"],
"q_id":"question25",
"correct/better_option":"",
"sign_up":"no",
"Daily_declaration":"no"}
},


{"question":{
"que":"Do you have senior citizens at home?",
"type":"Options",
"options":["Yes","No"],
"q_id":"question26",
"correct/better_option":"",
"sign_up":"no",
"Daily_declaration":"no"}
},
{"question":{
"que":"Do you have any one more than 65 years of age at home? ",
"type":"Options",
"options":["Yes","No"],
"q_id":"question27",
"correct/better_option":"",
"sign_up":"no",
"Daily_declaration":"no"}
},

{"question":{
"que":"I have downloaded, registered and regularly using Aarogya Setu application on my mobile phone.",
"type":"Options",
"options":["Yes","No"],
"q_id":"question28",
"correct/better_option":"Yes",
"sign_up":"no",
"Daily_declaration":"no"}
},
{"question":{
"que":"Any Other Information you wish to provide",
"type":"Single Entry",
"options":"",
"q_id":"question29",
"correct/better_option":"",
"sign_up":"no",
"Daily_declaration":"no"}
},
{"question":{
"que":"Your Age?",
"type":"Single Entry",
"options":"",
"q_id":"question30",
"correct/better_option":"",
"sign_up":"no",
"Daily_declaration":"no"}
},



{"question":{
"que":"What is the Pincode of your current staying place?",
"type":"Single Entry",
"options":"",
"q_id":"question31",
"correct/better_option":"",
"sign_up":"no",
"Daily_declaration":"no"}
},

{"question":{
"que":"Body Temperature",
"type":"Options",
"options":["Normal (96°F- 98.6°F)","Fever (98.6°F-102°F)","High Fever (>102°F)"],
"q_id":"question32",
"correct/better_option":"Normal (96°F- 98.6°F)",
"sign_up":"no",
"Daily_declaration":"no"}
},

{"question":{
"que":"Are you Experiencing Any of the Symptoms stated below?",
"type":"Multiple_Options",
"options":["Dry Cough","Sneezing","Sore throat","Weaknesss","None of these"],
"q_id":"question33",
"correct/better_option":["None of these"],
"sign_up":"no",
"Daily_declaration":"no"}
},

{"question":{
"que":"Please verify if you are experiencing any of the symptoms below",
"type":"Multiple_Options",
"options":["Moderate to Severe Cough","Feeling Breathlessness","Difficulty in Breathing","Drowsiness","Persistent Pain and Pressure in Chest","Severe Weakness","None of these"],
"q_id":"question34",
"correct/better_option":["None of these"],
"sign_up":"no",
"Daily_declaration":"no"}
},

{"question":{
"que":"Please Select your travel history (within past 21 days)",
"type":"Multiple_Options",
"options":["No travel history","Inter-city Travel","Inter-state Travel","Outside India"],
"q_id":"question35",
"correct/better_option":["No travel history"],
"sign_up":"no",
"Daily_declaration":"no"}
},


{"question":{
"que":"Do you have history of any of these conditions?",
"type":"Mulitple_Options",
"options":["Diabetes","High bloood pressure","Heart disease","kidney disease","Reduced immunity","None of these"],
"q_id":"question36",
"correct/better_option":["None of these"],
"sign_up":"no",
"Daily_declaration":"no"}
},


]


def fill_question_db():
    question_data_collection.insert_many(question_list)
    print("question db ready....")
fill_question_db()