'''import requests
import json

URL = 'https://www.sms4india.com/api/v1/sendCampaign'

# get request
def sendPostRequest(reqUrl, apiKey, secretKey, useType, phoneNo, senderId, textMessage):
  req_params = {
  'apikey':apiKey,
  'secret':secretKey,
  'usetype':useType,
  'phone': phoneNo,
  'message':textMessage,
  'senderid':senderId
  }
  return requests.post(reqUrl, req_params)

# get response
#response = sendPostRequest(URL, 'provided-api-key', 'provided-secret', 'prod/stage', 'valid-to-mobile', 'active-sender-id', 'message-text' )
#response = sendPostRequest(URL, '8QZZ067CDEVC8TRVLKUQ4X8S1LC2J9SE','BW745NL8IIOE4GHM','stage','8275906923','SMSIMD',"Your Anzen OTP is xxxxxx.")
#response = sendPostRequest(URL, 'BREK9U8IPBMD2A07BRZJEVVW07G29NGK','U3CCT3PW359WHSSJ','prod','9922998224','CYRNCS',"Your Anzen OTP is xxxxxx.")

"""
  Note:-
    you must provide apikey, secretkey, usetype, mobile, senderid and message values
    and then requst to api
"""
# print response if you want
#print (response.text)'''

from twilio.rest import Client


def send_otp(code,number):
    # Your Account Sid and Auth Token from twilio.com/console
    # DANGER! This is insecure. See http://twil.io/secure
    #account_sid = 'AC62c074cad2d81786eca5f45908af7450'
    #auth_token = '74f1ea7cdeabd5f91b3cc8268733a220'
    account_sid = 'ACa968d102710e848e5a1f1e4ec8127424'
    auth_token = '3494eeade6e2b1a73405dbb1239dc661'
    client = Client(account_sid, auth_token)

    message = client.messages \
        .create(
        body="Your OTP for ANZEN Health App is " + str(code) + " . Stay Safe !",
        from_="+13236723061",
        to='+91'+str(number)
    )
    print("In new code")    
    print(message.sid)

def send_qrcode(link,number):
    # Your Account Sid and Auth Token from twilio.com/console
    # DANGER! This is insecure. See http://twil.io/secure
    #account_sid = 'AC62c074cad2d81786eca5f45908af7450'
    #auth_token = '74f1ea7cdeabd5f91b3cc8268733a220'
    account_sid = 'ACa968d102710e848e5a1f1e4ec8127424'
    auth_token = '3494eeade6e2b1a73405dbb1239dc661'
    client = Client(account_sid, auth_token)

    message = client.messages \
        .create(
        body="Your visitor E-Pass for ANZEN Health App is generated. Please click on the following link to view."
             + str(link) + " . Stay Safe !",
        from_="+13236723061",
        to='+91'+str(number)
    )
    print(message.sid)

def send_signup_accept(status,number):
    # Your Account Sid and Auth Token from twilio.com/console
    # DANGER! This is insecure. See http://twil.io/secure
    #account_sid = 'AC62c074cad2d81786eca5f45908af7450'
    #auth_token = '74f1ea7cdeabd5f91b3cc8268733a220'
    account_sid = 'ACa968d102710e848e5a1f1e4ec8127424'
    auth_token = '3494eeade6e2b1a73405dbb1239dc661'
    client = Client(account_sid, auth_token)

    message = client.messages \
        .create(
        body="Your application for ANZEN Health App is " + status +  ". Stay Safe ! ",
        #from_='+14172753291',
        from_="+13236723061",
        to='+91'+str(number)
    )
    print(message.sid)


def send_signup_role(role,number):
    # Your Account Sid and Auth Token from twilio.com/console
    # DANGER! This is insecure. See http://twil.io/secure
    #account_sid = 'AC62c074cad2d81786eca5f45908af7450'
    #auth_token = '74f1ea7cdeabd5f91b3cc8268733a220'
    account_sid = 'ACa968d102710e848e5a1f1e4ec8127424'
    auth_token = '3494eeade6e2b1a73405dbb1239dc661'

    client = Client(account_sid, auth_token)

    message = client.messages \
        .create(
        body="Your role for ANZEN Health App has been updated to " + role +  ". Stay Safe ! ",
        #from_='+14172753291',
        from_="+13236723061",
        to='+91'+str(number)
    )
    print(message.sid)


def send_custom_message(message,numbers):

    #account_sid = 'AC62c074cad2d81786eca5f45908af7450'
    #auth_token = '74f1ea7cdeabd5f91b3cc8268733a220'
    account_sid = 'ACa968d102710e848e5a1f1e4ec8127424'
    auth_token = '3494eeade6e2b1a73405dbb1239dc661'

    client = Client(account_sid, auth_token)
    for number in numbers :
        message = client.messages \
            .create(
            body="ANZEN App Notification : " +  message,
            #from_='+14172753291',
            from_="+13236723061",
            to='+91'+str(number)
        )
        print(message.sid)


if __name__ == "__main__":
    send_otp(123456,"9922998224")